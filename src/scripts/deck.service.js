angular.module('durak')
.service('DeckService',[function(){
    return {
        initDeck: function (deckLength) {
            var deck = [];
            var values = [];
            var suites = ['hearts', 'tiles', 'clovers', 'pikes'];
            if (deckLength === 54){
                values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'];
                deck.push({suit: 'red', value: 'joker',_value:100});
                deck.push({suit: 'black', value: 'joker',_value:100});
            }else if (deckLength === 52){
                values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'];
            }else{
                values = [6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'];
            }
            angular.forEach(values, function (value,vInd) {
                angular.forEach(suites, function (suit, sInd) {
                    deck.push({suit: suit, _suit: sInd, value: value, _value: vInd});
                });
            });
            return deck;
        }
    };
}]);