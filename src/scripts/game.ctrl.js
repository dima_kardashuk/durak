angular.module('durak')
.controller('GameCtrl',
['$scope', 'GameService',
function ($scope, GameService) {

    $scope.game = {};
    $scope.startGame = function(){
        $scope.game = GameService.startGame();
    };

    //UI
    $scope.isMyTurn = function(){
        return $scope.game.isPlayerTurn(1);
    };
    $scope.isMyAttack = function(){
        return $scope.game.isPlayerAttack(1);
    };

    $scope.useCard = function (card) {
        if ($scope.isMyTurn()){
            if ($scope.isMyAttack()){
                $scope.game.attack(card);
            }else{
                $scope.game.defence(card);
            }
        }
    };

    $scope.isAllowedUse = function(){
        if ($scope.isMyTurn()){
            if ($scope.isMyAttack()){
                return $scope.game.isCurrentHasAllowedAttack();
            }else{
                return  $scope.game.isCurrentHasAllowedDefence();
            }
        }
    };

    $scope.sortHandBy = '_gameValue';
    //Main
    $scope.startGame();
}]);