angular.module('durak')
.service('GameAiService',
['$filter',
function($filter){
    var AiService = {
        player: 0,
        game: {},
        init: function(game){
            AiService.game = game;
            return AiService;
        },
        turn: function () {
            if (AiService.game.turnPlayer !== AiService.game.AI.player){
                var defence = AiService.game.AI.defence();
                if (defence){
                    AiService.game.defence(defence);
                }else{
                    AiService.game.take();
                }
            }else{
                var attack = AiService.game.AI.attack();
                if (attack){
                    AiService.game.attack(attack);
                }else{
                    AiService.game.discard();
                }
            }
        },
        attack: function () {
            var allowedAttack = [];
            angular.forEach(AiService.game.players[0].hand, function (card) {
                if (AiService.game.isAllowedAttack(card)){
                    allowedAttack.push(card);
                }
            });
            return allowedAttack ? $filter('orderBy')(allowedAttack,'_gameValue')[0] : null;
        },
        defence: function () {
            var allowedDefence = [];
            angular.forEach(AiService.game.players[0].hand, function (card) {
                if (AiService.game.isAllowedDefence(card)){
                    allowedDefence.push(card);
                }
            });
            return allowedDefence.length ? $filter('orderBy')(allowedDefence,'_gameValue')[0] : null;
        }
    };
    return AiService;
}]);