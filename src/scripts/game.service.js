angular.module('durak')
.service('GameService',
['$filter', 'DeckService', 'GameAiService',
function ( $filter, DeckService, GameAiService) {
    var Game = {};
    Game.deck = [];
    Game.trash = [];
    Game.table = [];
    Game.players = [];

    Game.initDeck = function(deckLength){
        Game.deck = DeckService.initDeck(deckLength);
    };

    Game.shuffleDeck = function () {
        Game.deck = $filter('shuffle')(Game.deck);
    };

    Game.startGame = function () {
        Game.trash = [];
        Game.table = [];
        Game.players = [{hand:[]},{hand:[]}];
        Game.initDeck();
        Game.shuffleDeck();
        Game.trump = Game.deck[0].suit;

        for(var i = 0; i<6; i++){
            Game.bankCard(0);
            Game.bankCard(1);
        }

        Game.startTurn( Math.random()>0.5?1:0 );
    };

    Game.bankCard = function(playerInd){
        if (Game.deck.length) {
            var card = Game.deck.splice(-1)[0];
            card._gameSuit = (card._suit+1) * (card.suit === Game.trump ? 10 : 1);
            card._gameValue = (card._value+1) * (card.suit === Game.trump ? 100 : 1);
            Game.players[playerInd].hand.push(card);
        }
    };

    Game.startTurn = function (playerInd) {
        Game.turnPlayer = playerInd;
        Game.changePlayer(playerInd);
    };
    Game.changePlayer = function (playerInd) {
        Game.currentPlayer = typeof playerInd !== 'undefined' ? playerInd : (Game.currentPlayer ? 0 : 1);
        //AI
        if (Game.currentPlayer === Game.AI.player){
            Game.AI.turn();
        }
    };
    Game.fillToSixCards = function (playerInd) {
        for (var i = 0; i <= 6-Game.players[playerInd].hand.length; i++){
            Game.bankCard(playerInd);
        }
    };
    Game.endTurn = function (skipTurn) {
        if(Game.players[Game.turnPlayer].hand.length < 6){
            Game.fillToSixCards(Game.turnPlayer);
        }
        if (Game.players[Game.turnPlayer?0:1].hand.length < 6){
            Game.fillToSixCards(Game.turnPlayer?0:1);
        }
        if (!Game.players[Game.turnPlayer].hand.length && !Game.players[Game.turnPlayer?0:1].hand.length){
            alert('Game ended');
        }else if (!Game.players[0].hand.length){
            alert('You lose');
        }else if (!Game.players[1].hand.length){
            alert('You win');
        }else {
            Game.startTurn(skipTurn ? Game.turnPlayer : (Game.turnPlayer ? 0 : 1));
        }
    };

    Game.discard = function () {
        Game.trash = Game.trash.concat(Game.table);
        Game.table = [];
        Game.endTurn(false);
    };

    Game.take = function () {
        Game.players[Game.currentPlayer].hand = Game.players[Game.currentPlayer].hand.concat( Game.table );
        Game.table = [];
        Game.endTurn(true);
    };

    Game.attack = function (card) {
        if (Game.isAllowedAttack(card)) {
            Game.table.push(
                Game.players[Game.currentPlayer].hand.splice(Game.players[Game.currentPlayer].hand.indexOf(card), 1)[0]
            );
            Game.changePlayer();
        }
    };

    Game.defence = function (card) {
        if (Game.isAllowedDefence(card)) {
            Game.table.push(
                Game.players[Game.currentPlayer].hand.splice(Game.players[Game.currentPlayer].hand.indexOf(card),1)[0]
            );
            Game.changePlayer();
        }
    };

    Game.isAllowedAttack = function (card) {
        var allowedValues = [];
        angular.forEach(Game.table, function(card){
            if (allowedValues.indexOf(card.value)===-1){
                allowedValues.push(card.value);
            }
        });
        return Game.table.length === 0 || allowedValues.indexOf(card.value) !== -1;
    };
    Game.isAllowedDefence = function(card){
        if (!Game.table.length) return false;
        var top = Game.table[Game.table.length-1];
        return top.suit === Game.trump
            ? card.suit === Game.trump && card._value > top._value
            : card.suit === Game.trump || (card.suit === top.suit && card._value > top._value);
    };

    Game.isPlayerTurn = function(playerInd){
        return Game.currentPlayer === playerInd;
    };
    Game.isPlayerAttack = function(playerInd){
        return Game.turnPlayer === playerInd;
    };
    
    Game.isCurrentHasAllowedAttack = function(){
        for(var ci in  Game.players[Game.currentPlayer].hand){
            if (Game.isAllowedAttack( Game.players[Game.currentPlayer].hand[ci] )){
                return true;
            }
        }
        return false;
    };
    Game.isCurrentHasAllowedDefence = function(){
        for(var ci in  Game.players[Game.currentPlayer].hand){
            if (Game.isAllowedDefence( Game.players[Game.currentPlayer].hand[ci] )){
                return true;
            }
        }
        return false;
    };

    Game.AI = GameAiService.init(Game);

    return {
        startGame: function(){
            var game = Game;
            game.startGame();
            return game;
        }
    };
}]);